<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
?><!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" <?php language_attributes(); ?>>
<head>
<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
<title><?php woo_title(); ?></title>
<?php woo_meta(); ?>
<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . '/fonts/substance/MyFontsWebfontsKit.css'; ?>">
<?php wp_head(); ?>
<?php woo_head(); ?>
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
$('hgroup#logo').waypoint('sticky', {
  offset: 3 // Apply "stuck" when element (X)px from top
});
});
    </script>
    
<script type="text/javascript">
	jQuery(document).ready(function ($) {    
	$('#social-menu-container').waypoint('sticky', {
  offset: 20 // Apply "stuck" when element 1px from top
});
});
    </script>

<script type="text/javascript">
	jQuery(document).ready(function ($) {    
	$('form#searchform.stuck').waypoint('sticky', {
  offset: 70 // Apply "stuck" when element 1px from top
});
});
    </script>

</head>
<body <?php body_class(); ?>>
				
<?php woo_top(); ?>


	<div id="inner-wrapper">
<div id="wrapper">
		         

	<?php woo_header_before(); ?>



     
	<header id="header" class="col-full">

		<?php woo_header_inside(); ?>


	</header>
	<?php woo_header_after(); ?>
	<script>
            new UISearch( document.getElementById( 'sb-search' ) );
        </script>
