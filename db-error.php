<?php
header( 'HTTP/1.1 503 Service Temporarily Unavailable' );
header( 'Status: 503 Service Temporarily Unavailable' );
header( 'Retry-After: 3600' ); // 60 minutes
mail( 'justin+alert@travelertech.com', 'Database Error!', 'Traveler Tech just had a database error… better check things out!', 'From: Traveler Tech Alert' );
?>
<!DOCTYPE html>
<html lang="en-US" dir="ltr">
<head>
	<title>We're Off The Flight Line</title>
	<meta charset="utf-8" />
	<style>
	body {
		background: #fff;
		font: 16px Georgia, serif;
		line-height: 1.3;
		margin: 0;
		padding: 0;
	}
	#image{
	float: left;
	height: 100%;
	}

	#content {
		background: #fff ;
		margin: auto 0;
		padding: 50px;
	}

	h1 {
		font-size: 34px;
		font-weight: normal;
		margin-top: 0;
	}

	p {
		margin: 0 0 10px 5px;
	}
	</style>
</head>
<body>

<div id="content">
<div id="image">
<img src="/wp-content/themes/tt-main/images/maintenance.png"></img> 
</div>
	<h1>We're experiencing some severe turbulence…</h1>
	<p>Traveler Tech is having a bit of trouble right now, but we're working on the problem.</p>
	<p>Check back here soon… We should be back up in the air before you know it!</p>
</div><!-- #content -->
</body>
</html>
<?php die(); ?>
