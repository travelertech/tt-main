<?php
$protocol = $_SERVER["SERVER_PROTOCOL"];
if ( 'HTTP/1.1' != $protocol && 'HTTP/1.0' != $protocol ) $protocol = 'HTTP/1.0';
header( "$protocol 503 Service Unavailable", true, 503 );
header( 'Content-Type: text/html; charset=utf-8' );
header( 'Retry-After: 600' ); // 10 minutes
?>
<!DOCTYPE html>
<html lang="en-US" dir="ltr">
<head>
	<title>We're Off The Flight Line</title>
	<meta charset="utf-8" />
	<style>
	body {
		background: #fff;
		font: 16px Georgia, serif;
		line-height: 1.3;
		margin: 0;
		padding: 0;
	}
	#image{
	float: left;
	height: 100%;
	}

	#content {
		background: #fff ;
		margin: auto 0;
		padding: 50px;
	}

	h1 {
		font-size: 34px;
		font-weight: normal;
		margin-top: 0;
	}

	p {
		margin: 0 0 10px 5px;
	}
	</style>
</head>
<body>

<div id="content">
<div id="image">
<img src="/wp-content/themes/tt-main/images/maintenance.png"></img> 
</div>
	<h1>We've pulled into the hanger for a quick tune-up.</h1>
	<p>Traveler Tech is doing a bit of site maintenance that should only last a few minutes.</p>
	<p>Check back soon… We'll be back up in the air before you know it!</p>
</div><!-- #content -->

</body>
</html>
<?php die(); ?>