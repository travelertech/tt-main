<?php
/**
 * Error Content Template
 *
 * This template is the content template for error screens. It is used to display a message
 * to the viewer when no appropriate page can be found by WordPress.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */

 $title_before = '<h1 class="title">';
 $title_after = '</h1>';

 $page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

 woo_post_before();
?>
<article <?php post_class(); ?>>
<?php woo_post_inside_before();	?>

	<header>
	 <div id="test">
		<?php echo $title_before . apply_filters( 'woo_404_title', __( 'Hit Some Turbulance?', 'woothemes' ) ) . $title_after; ?>

		<script src="/wp-content/themes/tt-main/js/departure-board.js"></script>
		
		<script>
		
			var board = new DepartureBoard (document.getElementById ('test'), { rowCount: 2, letterCount: 45 }); 
			board.setValue ([' FLIGHT 404				   Traveler Tech		     DELAYED', ' Page not found!']);
			
			window.setTimeout (function () {
			 	board.setValue ([' Alternate Flights Available Below', '']);
			}, 12000);

		</script>
		</div>
	</header>

	<section class="entry">
	<p>Our automated gate agent is happy to assist you…</p>
		<?php 
			$s = preg_replace("/(.*)-(html|htm|php|asp|aspx)$/","$1",$wp_query->query_vars['name']);
			$posts = query_posts('post_type=any&name='.$s);
			$s = str_replace("-"," ",$s);
			if (count($posts) == 0) {
				$posts = query_posts('post_type=any&s='.$s);
			}
			if (count($posts) > 0) {
				echo "<ol><li>";
				echo "<p>Were you looking for <strong>one of the following</strong> posts or pages?</p>";
				echo "<ul>";
				foreach ($posts as $post) {
					echo '<li><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a></li>';
				}
				echo "</ul>";
				echo "<p>If not, here are a few more options to help you get to your destination:</p></li>";
			} else {
				echo "<p><strong>Here are a few options to help you get to your destination:</p>";
				echo "<ol>";
			}
		?>
			<li>
				<label for="s"><strong>Search</strong> for it:</label>
				<form style="display:inline;" action="<?php bloginfo('siteurl');?>">
					<input type="text" value="<?php echo esc_attr($s); ?>" id="s" name="s"/> <input type="submit" value="Search"/>
				</form>
			</li>
			<li>
				<strong>If you typed in a URL...</strong> make sure the spelling, cApitALiZaTiOn, and punctuation are correct. Then try reloading the page.
				
			</li>
			<li>
				<strong>Look</strong> for it in the <a href="<?php bloginfo('siteurl');?>/sitemap/">sitemap</a>.
				
			</li>
			<li>
				<strong>Start over again</strong> at my <a href="<?php bloginfo('siteurl');?>">homepage</a> (and please contact me to say what went wrong, so I can fix it).
			</li>
			
		</ol>								

	</section><!-- /.entry -->
<?php
	woo_post_inside_after();
?>
</article><!-- /.post -->
<?php
	woo_post_after();
?>