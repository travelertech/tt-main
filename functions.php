<?php

// ADD SOCIAL MEDIA NETWORKS TO USER PROFILES
function extra_contact_info($contactmethods) {

	unset($contactmethods['aim']);

	unset($contactmethods['yim']);

	unset($contactmethods['jabber']);

	$contactmethods['facebook'] = 'Facebook';

	$contactmethods['twitter'] = 'Twitter';

	$contactmethods['linkedin'] = 'LinkedIn';

	$contactmethods['appdotnet'] = 'App.net';

	$contactmethods['googleplus'] = 'Google+';

	return $contactmethods;

}
add_filter('user_contactmethods', 'extra_contact_info');



// FUNCTIONS FOR REVIEW PAGES

	// RENAME EXISTING TABS
	add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
	function woo_rename_tabs( $tabs ) {
	$tabs['description']['title'] = __( 'Traveler Tech Review' );		// Rename the description tab
	$tabs['reviews']['title'] = __( 'User Reviews' );				// Rename the reviews tab
	$tabs['additional_information']['title'] = __( 'Product Data' );	// Rename the additional information tab
	return $tabs;
}
	
	// ADD TAB FOR IMAGE GALLERY
	add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
	
	// Adds the new tab
	$tabs['gallery_tab'] = array(
		'title' 	=> __( 'Image Gallery', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_new_product_tab_content'
	);
	return $tabs;
}

function woo_new_product_tab_content() {

	// Adds the Gallery…
do_action('woocommerce_product_thumbnails'); 	
}
	
// POST FORMATS
add_theme_support( 'post-formats', array(
    'link', 'video', 'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status'
) );


// REMOVE THIRSTY AFFILIATES MEDIA UPLOADER 
add_action( 'admin_enqueue_scripts', 'ta_remove_wf_media_assets' );
 
function ta_remove_wf_media_assets ( $hook ) {
	if ( 'post.php' == $hook && 'thirstylink' == get_post_type() ) {
		remove_action( 'admin_print_styles', 'woothemes_mlu_css', 0 );
		remove_action( 'admin_print_scripts', 'woothemes_mlu_js', 0 );
	}
} 

// ADD LOGIN/LOGOUT IN TOP MENU (USES SIMPLEMODAL LOGIN PLUGIN)
add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );

function add_loginout_link( $items, $args ) {
    if (is_user_logged_in() && $args->theme_location == 'top-menu') {
     	$class = 'loggedin';
     	$current_user = wp_get_current_user();
        $items .= '<li class="loggedin"><a class="' . $class . '" href="'. wp_logout_url() .'">' . $current_user->display_name . '</a></li>';
    }
    elseif (!is_user_logged_in() && $args->theme_location == 'top-menu') {
        $class = 'simplemodal-login';
        $redirect = $_SERVER['REQUEST_URI'];  
        $items .= '<li class="loggedout"><a class="' . $class . '" href="'. site_url('wp-login.php?redirect_to='. $redirect ) .'">Log In / Register</a></li>';
    }
    return $items;
}

// CUSTOM MODAL LOGIN/LOGOUT (USES SIMPLEMODAL LOGIN PLUGIN)
add_filter('simplemodal_login_form', 'mytheme_login_form');

function mytheme_login_form($form) {
	$users_can_register = get_option('users_can_register') ? true : false;
	$options = get_option('simplemodal_login_options');

	$output = sprintf('
<form name="loginform" id="loginform" action="%s" method="post">
	<hgroup id="modal">
<a href="http://travelertech.dev/" title="Tools for Modern-Day Explorers"><img src="http://travelertech.dev/wp-content/uploads/2013/07/TT_Temp_SM.jpg" alt="Traveler Tech" /></a>
<h1 class="site-title"><a href="http://travelertech.dev/">Traveler Tech</a></h1>
<span class="site-description">Tools for Modern-Day Explorers</span>
</hgroup>
	<div class="simplemodal-login-fields">
	<p>
		<label>%s<br />
		<input type="text" name="log" class="user_login input" value="" size="20" tabindex="10" /></label>
	</p>
	<p>
		<label>%s<br />
		<input type="password" name="pwd" class="user_pass input" value="" size="20" tabindex="20" /></label>
	</p>',
		site_url('wp-login.php', 'login_post'),
		__('Login', 'simplemodal-login'),
		__('Username', 'simplemodal-login'),
		__('Password', 'simplemodal-login')
	);

	ob_start();
	do_action('login_form');
	$output .= ob_get_clean();

	$output .= sprintf('
	<p class="forgetmenot"><label><input name="rememberme" type="checkbox" id="rememberme" class="rememberme" value="forever" tabindex="90" />%s</label></p>
	<p class="submit">
		<input type="submit" name="wp-submit" value="%s" tabindex="100" />
		<input type="button" class="simplemodal-close" value="%s" tabindex="101" />
		<input type="hidden" name="testcookie" value="1" />
	</p>
	<p class="nav">',
		__('Remember Me', 'simplemodal-login'),
		__('Log In', 'simplemodal-login'),
		__('Cancel', 'simplemodal-login')
	);

	if ($users_can_register && $options['registration']) {
		$output .= sprintf('<a class="simplemodal-register" href="%s">%s</a>', 
			site_url('wp-login.php?action=register', 'login'), 
			__('Register', 'simplemodal-login')
		);
	}

	if (($users_can_register && $options['registration']) && $options['reset']) {
		$output .= ' | ';
	}

	if ($options['reset']) {
		$output .= sprintf('<a class="simplemodal-forgotpw" href="%s" title="%s">%s</a>',
			site_url('wp-login.php?action=lostpassword', 'login'),
			__('Password Lost and Found', 'simplemodal-login'),
			__('Lost your password?', 'simplemodal-login')
		);
	}

	$output .= ' 
	</p>
	</div>
	<div class="simplemodal-login-activity" style="display:none;"></div>
</form>';

	return $output;
}

// CUSTOM MODAL REGISTRATION (USES SIMPLEMODAL LOGIN PLUGIN)
add_filter('simplemodal_registration_form', 'mytheme_registration_form');
function mytheme_registration_form($form) {
	$options = get_option('simplemodal_login_options');

	$output = sprintf('
<form name="registerform" id="registerform" action="%s" method="post">
	<div class="title">%s OHBOYOHBOY</div>
	<div class="simplemodal-login-fields">
	<p>
		<label>%s<br />
		<input type="text" name="user_login" class="user_login input" value="" size="20" tabindex="10" /></label>
	</p>
	<p>
		<label>%s<br />
		<input type="text" name="user_email" class="user_email input" value="" size="25" tabindex="20" /></label>
	</p>',
		site_url('wp-login.php?action=register', 'login_post'),
		__('Register', 'simplemodal-login'),
		__('Username', 'simplemodal-login'),
		__('E-mail', 'simplemodal-login')
	);

	ob_start();
	do_action('register_form');
	$output .= ob_get_clean();

	$output .= sprintf('
	<p class="reg_passmail">%s</p>
	<p class="submit">
		<input type="submit" name="wp-submit" value="%s" tabindex="100" />
		<input type="button" class="simplemodal-close" value="%s" tabindex="101" />
	</p>
	<p class="nav">
		<a class="simplemodal-login" href="%s">%s</a>',
		__('A password will be e-mailed to you.', 'simplemodal-login'),
		__('Register', 'simplemodal-login'),
		__('Cancel', 'simplemodal-login'),
		site_url('wp-login.php', 'login'),
		__('Log in', 'simplemodal-login')
	);

	if ($options['reset']) {
		$output .= sprintf(' | <a class="simplemodal-forgotpw" href="%s" title="%s">%s</a>',
			site_url('wp-login.php?action=lostpassword', 'login'),
			__('Password Lost and Found', 'simplemodal-login'),
			__('Lost your password?', 'simplemodal-login')
		);
	}

	$output .= '
	</p>
	</div>
	<div class="simplemodal-login-activity" style="display:none;"></div>
</form>';

	return $output;
}


 // Pretty Photo Script
add_action( 'wp_enqueue_scripts', 'lightbox' );
	function lightbox() {
	global $woocommerce;
	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	{
	wp_enqueue_script( 'prettyPhoto', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
	wp_enqueue_script( 'prettyPhoto-init', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto.init' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
	wp_enqueue_style( 'woocommerce_prettyPhoto_css', $woocommerce->plugin_url() . '/assets/css/prettyPhoto.css' );
	}
}


// Pretty Photo - adding the rel to all images on site
function autoadd_rel_prettyPhoto($content) {
    global $post;
    $pattern        = "/(<a(?![^>]*?rel=['\"]prettyPhoto.*)[^>]*?href=['\"][^'\"]+?\.(?:bmp|gif|jpg|jpeg|png)['\"][^\>]*)>/i";
    $replacement    = '$1 rel="prettyPhoto['.$post->ID.']">';
    $content = preg_replace($pattern, $replacement, $content);
    return $content;
}
add_filter("the_content","autoadd_rel_prettyPhoto");


/*-----------------------------------------------------------------------------------*/
/*	Javascsript
/*-----------------------------------------------------------------------------------*/

// WAYPOINT SCRIPTS
wp_register_script('waypoints', get_stylesheet_directory_uri() . '/js/waypoints.min.js', array('jquery'),  true );
wp_enqueue_script('waypoints');

wp_register_script('stickywaypoints', get_stylesheet_directory_uri() . '/js/waypoints-sticky.min.js', array('jquery'),  true );
wp_enqueue_script('stickywaypoints');


// SEARCH SCRIPTS

add_action('wp_enqueue_scripts','search_scripts_function');
function search_scripts_function() {
	//get theme options
	global $options;
	
	wp_enqueue_script('jquery');	
	

	wp_enqueue_script('classie', get_stylesheet_directory_uri() . '/js/classie.js');
	wp_enqueue_script('uisearch', get_stylesheet_directory_uri() . '/js/uisearch.js');
	wp_enqueue_script('modernizr.custom', get_stylesheet_directory_uri() . '/js/modernizr.custom.js');

}



/*-----------------------------------------------------------------------------------*/
/*	Social Menu
/*-----------------------------------------------------------------------------------*/

// Add custom menu location for new menu(s)
add_action( 'init', 'woo_custom_add_menu_locations', 10 );
 
function woo_custom_add_menu_locations () {
 
 // Create an array of the menu locations we'd like to register. This example just has one menu location
 $menus = array(
 'social-menu' => __( 'Social Menu', 'woothemes' )// Note, there is no comma after the last item in the array.
 );
 
 // Call the WordPress register_nav_menus() function and pass it our array of menu locations.
 register_nav_menus( $menus );
 
} // End woo_custom_add_menu_locations()

// Load the new menu into the theme after the main nav
add_action( 'woo_header_after', 'woo_custom_add_secondary_menu', 10 );
 
function woo_custom_add_secondary_menu () {
 
 $menu_location = 'social-menu'; // Change this to be whichever menu location you want to add, provided it matches your menu slug
 
 if ( has_nav_menu( $menu_location ) ) {
 echo '
<div id="' . $menu_location . '-container" class="col-full custom-menu-location">' . "
";
 wp_nav_menu( array( 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => $menu_location, 'menu_class' => 'nav fl', 'theme_location' => $menu_location ) );
 echo '</div>
 
<!--/#' . $menu_location . '-container .col-full-->' . "
";
 }
 
} // End woo_custom_add_secondary_menu()

// ADD SOCIAL BUTTONS IN MENU
add_filter( 'wp_nav_menu_items', 'woo_custom_add_sociallink_navitems', 10, 2 );
 
function woo_custom_add_sociallink_navitems ( $items, $args ) {
    global $woo_options;
 
    if ( $args->theme_location == 'social-menu' ) {


        $template_directory = get_template_directory_uri();
 
        $profiles = array(
                        'twitter' => __( 'Twitter' , 'woothemes' ),
                        'facebook' => __( 'Facebook' , 'woothemes' ),
                        'youtube' => __( 'YouTube' , 'woothemes' ),
                        'adn' => __( 'App.net' , 'woothemes' ),
                        'linkedin' => __( 'LinkedIn' , 'woothemes' ),
                        'rss' => __( 'RSS' , 'woothemes' ),
                        'googleplus' => __( 'Google+' , 'woothemes' )
                    );
 
        foreach ( $profiles as $key => $text ) {
            if ( isset( $woo_options['woo_connect_' . $key] ) && $woo_options['woo_connect_' . $key] != '' ) {
                $class = 'icon-' . $key;
                $items .= '<li class="social-menu-item"><a href="' . $woo_options['woo_connect_' . $key] . '" title="' . esc_attr( $text ) . '"><i class="'. $class .'"></i><span class="screen-reader-text">' . esc_attr( $text ) . '</span></a></li>' . "
";
            }
        }
    }
 
    return  $items ;
}



/*-----------------------------------------------------------------------------------*/
/*	Bootstrap Menu
/*-----------------------------------------------------------------------------------*/

//Bootstrap Nav
add_action( 'after_setup_theme', 'bootstrap_setup' );
 
if ( ! function_exists( 'bootstrap_setup' ) ):
 
function bootstrap_setup(){
 
add_action( 'init', 'register_menu' );
function register_menu(){
register_nav_menu( 'primary', 'Bootstrap Top Menu' );
}
 
class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {
 
function start_lvl( &$output, $depth ) {
 
$indent = str_repeat( "\t", $depth );
$output	.= "\n$indent<ul class=\"dropdown-menu\">\n";
}
 
function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
$li_attributes = '';
$class_names = $value = '';
 
$classes = empty( $item->classes ) ? array() : (array) $item->classes;
$classes[] = ($args->has_children) ? 'dropdown' : '';
$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
$classes[] = 'menu-item-' . $item->ID;
 
 
$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
$class_names = ' class="' . esc_attr( $class_names ) . '"';
 
$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
 
$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
 
$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
$attributes .= ($args->has_children) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
 
$item_output = $args->before;
$item_output .= '<a'. $attributes .'>';
$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
$item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
$item_output .= $args->after;
 
$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}
 
function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
if ( !$element )
return;
$id_field = $this->db_fields['id'];
 
//display this element
if ( is_array( $args[0] ) )
$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
else if ( is_object( $args[0] ) )
$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
$cb_args = array_merge( array(&$output, $element, $depth), $args);
call_user_func_array(array(&$this, 'start_el'), $cb_args);
 
$id = $element->$id_field;
 
// descend only when the depth is right and there are childrens for this element
if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {
 
foreach( $children_elements[ $id ] as $child ){
 
if ( !isset($newlevel) ) {
$newlevel = true;
//start the child delimiter
$cb_args = array_merge( array(&$output, $depth), $args);
call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
}
$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
}
unset( $children_elements[ $id ] );
}
 
if ( isset($newlevel) && $newlevel ){
//end the child delimiter
$cb_args = array_merge( array(&$output, $depth), $args);
call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
}
 
//end this element
$cb_args = array_merge( array(&$output, $element, $depth), $args);
call_user_func_array(array(&$this, 'end_el'), $cb_args);
}
}
 
}
 
endif;

/*
// COMMENTED OUT
// SEARCH 
function add_search_to_wp_menu ( $items, $args ) {
	if( 'primary-menu' === $args -> theme_location ) {
//$items .= '<li class="menu-item menu-item-search">';
$items .= '<div id="sb-search" class="sb-search">';
$items .= '<form role="search" method="get" id="searchform" action="' . get_bloginfo('home') . '/">';
$items .= '<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">';
$items .= '<input class="sb-search-submit" type="submit" value="">';
$items .= '<span class="sb-icon-search"></span>';
$items .= '</form>';
$items .= '</div>';
//$items .= '</li>';

	}
	
return $items;

}
add_filter('wp_nav_menu_items','add_search_to_wp_menu',10,2);


// SEARCH 
// SEARCH 
function add_search_to_wp_menu ( $items, $args ) {
	if( 'top-menu' === $args -> theme_location ) {
//$items .= '<li class="menu-item menu-item-search">';
$items .= '<div id="sb-search" class="sb-search">';
$items .= '<form role="search" method="get" id="searchform" action="' . get_bloginfo('home') . '/">';
$items .= '<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">';
$items .= '<input class="sb-search-submit" type="submit" value="">';
$items .= '<span class="sb-icon-search"></span>';
$items .= '</form>';
$items .= '</div>';
//$items .= '</li>';

	}
	
return $items;

}
add_filter('wp_nav_menu_items','add_search_to_wp_menu',10,2);

function add_search_to_wp_menu ( $items, $args ) {
	if( 'top-menu' === $args -> theme_location ) {
$items .= '<li class="menu-item menu-item-search">';
$items .= '<div id="sb-search" class="sb-search">';
$items .= '<form role="search" method="get" id="searchform" action="' . get_bloginfo('home') . '/">';
$items .= '<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">';
$items .= '<input class="sb-search-submit" type="submit" value="">';
$items .= '<span class="sb-icon-search"></span>';
$items .= '</form>';
$items .= '</div>';
$items .= '</li>';

	}
	
return $items;

}
add_filter('wp_nav_menu_items','add_search_to_wp_menu',10,2);


// SEARCH 
function add_search_to_wp_menu ( $items, $args ) {
	if( 'top-menu' === $args -> theme_location ) {
$items .= '<li class="menu-item menu-item-search">';
$items .= '<div id="sb-search" class="sb-search">';
$items .= '<form role="search" method="get" id="searchform" action="' . get_bloginfo('home') . '/">';
$items .= '<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">';
$items .= '<input class="sb-search-submit" type="submit" value="">';
$items .= '<span class="sb-icon-search"></span>';
$items .= '</form>';
$items .= '</div>';
$items .= '</li>';

	}
	
return $items;

}


add_filter('wp_nav_menu_items','add_search_to_wp_menu',10,2);
$items .= '<script>new UISearch( document.getElementById( 'sb-search' ) );</script>';

<script>new UISearch( document.getElementById( 'sb-search' ) );</script>



<div id="sb-search" class="sb-search">
						<form>
							<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<div id="sb-search" class="sb-search">
						<form>
							<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search"></span>
						</form>
					</div>
						</form>
					</div>
/* SEARCH 
function add_search_to_wp_menu ( $items, $args ) {
	if( 'top-menu' === $args -> theme_location ) {
$items .= '<li class="menu-item menu-item-search">';
$items .= '<form method="get" class="menu-search-form" action="' . get_bloginfo('home') . '/"><p><input class="text_input" type="text" value="Enter Text &amp; Click to Search" name="s" id="s" onfocus="if (this.value == \'Enter Text &amp; Click to Search\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \'Enter Text &amp; Click to Search\';}" /><input type="submit" class="my-wp-search" id="searchsubmit" value="search" /></p></form>';
$items .= '</li>';
	}
return $items;
}
add_filter('wp_nav_menu_items','add_search_to_wp_menu',10,2);
//SEARCH
add_action( 'woo_header_before', 'woo_custom_add_searchform', 10 );
 
function woo_custom_add_searchform () {
    echo '<div id="nav-search" class="nav-search fr">' . "
";
    get_template_part( 'search', 'form' );
    echo '</div><!--/#nav-search .nav-search fr-->' . "
";
} // End woo_custom_add_searchform()


    // Add main nav to the woo_header_inside hook

add_action( 'init', 'woo_custom_move_navigation', 10 );

function woo_custom_move_navigation () {
    // Remove main nav from the woo_header_after hook
    remove_action( 'woo_header_after','woo_nav', 10 );

    // Add main nav to the woo_header_inside hook
    add_action( 'woo_header_inside','woo_nav', 10 );
} // End woo_custom_move_navigation()

// ADD SOCIAL BUTTONS IN TOP MENU
add_filter( 'wp_nav_menu_items', 'woo_custom_add_sociallink_navitems', 10, 2 );
 
function woo_custom_add_sociallink_navitems ( $items, $args ) {
    global $woo_options;
 
    if ( $args->theme_location == 'top-menu' ) {
 
        $template_directory = get_template_directory_uri();
 
        $profiles = array(
                        'twitter' => __( 'Twitter' , 'woothemes' ),
                        'facebook' => __( 'Facebook' , 'woothemes' ),
                        'youtube' => __( 'YouTube' , 'woothemes' ),
                        'adn' => __( 'App.net' , 'woothemes' ),
                        'linkedin' => __( 'LinkedIn' , 'woothemes' ),
                        'rss' => __( 'RSS' , 'woothemes' ),
                        'googleplus' => __( 'Google+' , 'woothemes' )
                    );
 
        foreach ( $profiles as $key => $text ) {
            if ( isset( $woo_options['woo_connect_' . $key] ) && $woo_options['woo_connect_' . $key] != '' ) {
                $class = 'icon-' . $key;
                $items .= '<li class="social-menu-item"><a href="' . $woo_options['woo_connect_' . $key] . '" title="' . esc_attr( $text ) . '"><i class="'. $class .'"></i><span class="screen-reader-text">' . esc_attr( $text ) . '</span></a></li>' . "
";
            }
        }
    }
 
    return $items;
}
// FORMAT CHAT TRANSCRIPTS
function styling_chat_post($content) {
	global $post; 
	if (has_post_format('chat')) { 
		remove_filter ('the_content', 'wpautop'); 
		$chatoutput = "<ul class=\"chat\">\n"; 
		$split = preg_split("/(\r?\n)+|(<br\s*\/?>\s*)+/", $content); 
		foreach($split as $haystack) { 
			if (strpos($haystack, ":")) { 
				$string = explode(":", trim($haystack), 2); 
				$who = strip_tags(trim($string[0])); 
				$what = strip_tags(trim($string[1])); 
				$row_class = empty($row_class)? " class=\"chat-highlight\"" : ""; 
				$chatoutput = $chatoutput . "<li><span class="name">$who</span><p>$what</p></li>\n"; 
			} else { 
				$chatoutput = $chatoutput . $haystack . "\n"; 
			} 
		}   

		// print our new formated chat post 
		$content = $chatoutput . "</ul>\n"; 
		return $content; 
	} else { 
		return $content; 
	} 
} 

add_filter( "the_content", "styling_chat_post", 9);


//SEARCH
 
function woo_custom_add_searchform ( $items, $args ) {
 global $woo_options;
 
    if ( $args->theme_location == 'top-menu' ) {

    echo '<div id="nav-search" class="nav-search fr">' . "
";
    get_template_part( 'search', 'form' );
    echo '</div><!--/#nav-search .nav-search fr-->' . "
";

        }
} // End woo_custom_add_searchform()

add_filter( 'wp_nav_menu_items', 'woo_custom_add_searchform', 10, 2);

*/
