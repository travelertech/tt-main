<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
?><!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" <?php language_attributes(); ?>>
<head>
<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
<title><?php woo_title(); ?></title>
<?php woo_meta(); ?>
<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() . '/fonts/substance/MyFontsWebfontsKit.css'; ?>">
<?php wp_head(); ?>
<?php woo_head(); ?>
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>



</head>
<body <?php body_class(); ?>>

<?php woo_top(); ?>

<header id="header" class="col-full">

<div class="navbar navbar-default navbar-static-top">
					<?php woo_header_inside(); ?>

    			

			    <div class="navbar-collapse collapse">

				<?php
				$args = array(
				'depth'	=> 2,
				'container'	=> false,
				'menu_class'	=> 'nav navbar-nav',
				'walker'	=> new Bootstrap_Walker_Nav_Menu()
				);
				 
				wp_nav_menu($args);
				?>

				

				</div><!-- /.nav-collapse -->
		</div><!-- /.navbar -->
			</header>

<div id="wrapper">
	
	<div id="inner-wrapper">

	<?php woo_header_before(); ?>


	
		
	<?php woo_header_after(); ?>