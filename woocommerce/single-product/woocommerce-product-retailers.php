<?php
/**
 * Plugin Name: WooCommerce Product Retailers
 * Plugin URI: http://www.woothemes.com/products/woocommerce-product-retailers/
 * Description: Allow customers to purchase products from external retailers
 * Author: SkyVerge
 * Author URI: http://www.skyverge.com
 * Version: 1.0
 * Text Domain: wc-product-retailers
 * Domain Path: /languages/
 *
 * Copyright: (c) 2013 SkyVerge, Inc. (info@skyverge.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Product-Retailers
 * @author    SkyVerge
 * @copyright Copyright (c) 2013, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Required functions
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

// Plugin updates
woothemes_queue_update( plugin_basename( __FILE__ ), '9766af75222eed8f4fcdf56263685d41', '187888' );

// Check if WooCommerce is active and deactivate extension if it's not
if ( ! is_woocommerce_active() )
	return;

/**
 * The WC_Product_Retailers global object
 * @name $wc_product_retailers
 * @global WC_Product_Retailers $GLOBALS['wc_product_retailers']
 */
$GLOBALS['wc_product_retailers'] = new WC_Product_Retailers();


/**
 * <h2>WooCommerce Product Retailers Plugin Class</h2>
 *
 * <h3>Plugin Overview</h3>
 *
 * This plugin allows admins to create a list of retailers which can then be
 * assigned to products along with a URL, to be displayed on the frontend
 * product page as a button or dropdown list as an affiliate/external purchase
 * option.  Products can be configured to be purchasable both on site and through
 * the retailers, as well as only through the retailers, resulting in a more full
 * featured "affiliate/external" product functionality that can be used with
 * simple as well as variable product types
 *
 * <h3>Terminology</h3>
 *
 * Despite the plugin name of "product retailers", "retailers" is used internally
 * to refer to the retailers.
 *
 * <h3>Admin Considerations</h3>
 *
 * This plugin adds a "Retailers" menu item to the "WooCommerce" top level menu
 * where the retailers post type is managed.
 *
 * Global settings for this plugin are added to the Catalog tab under "Product
 * Retailers"
 *
 * Within the product admin a new admin panel named "Retailers" is added to the
 * Product Data panel with overrides for the global settings, and a retailers
 * list for managing retailers for the product.
 *
 * <h3>Frontend Considerations</h3>
 *
 * On the catalog page the "add to cart" button for simple products which are
 * sold only through retailers is altered to link directly to the product page
 * like a variable product, rather than performing an AJAX add to cart.
 *
 * On the product page if there is a single retailer it is displayed as a button
 * with configurable text.  If there is more than one retailer, they are displayed
 * as a dropdown list, which when selected redirects the client on to the
 * configured URL for purchase.
 *
 * Variations for variable products which are sold only through retailers are
 * displayed regardless of whether a price is configured (usually they are not
 * shown if there is no price set).
 *
 * <h3>Database</h3>
 *
 * <h4>Options table</h4>
 *
 * **wc_product_retailers_version** - the current plugin version, set on install/upgrade
 *
 * **wc_product_retailers_product_button_text** - Text shown on the dropdown/
 *   button linking to the external URL, unless overridden at the product level
 *
 * **wc_product_retailers_catalog_button_text** - Text shown on the catalog page
 *   "Add to Cart" button for simple products which are sold only through retailers,
 *   unless overridden at the product levle.
 *
 * <h4>Custom Post Type</h4>
 *
 * **wc_product_retailer** - A Custom Post Type which represents a retailer
 *
 * <h4>Retailer CPT Postmeta</h4>
 *
 * **_product_retailer_default_url** - (string) optional retailer URL, used
 *   unless overridden at the product level
 *
 * <h4>Product Postmeta</h4>
 *
 * **_wc_product_retailers_retailer_only_purchase** - Indicates whether the product
 *   is available for purchase only from retailers
 *
 * **_wc_product_retailers_product_button_text** - Optionally overrides the
 *   global wc_product_retailers_product_button_text setting
 *
 * **_wc_product_retailers_catalog_button_text** - Optionally overrides the
 *   global wc_product_retailers_catalog_button_text setting
 *
 * **_wc_product_retailers** - array of assigned retailers, with the following
 * data structure:
 * <pre>
 * Array(
 *   id          => (int) retailer id,
 *   product_url => (string) optional product url,
 *
 * )
 * </pre>
 */
class WC_Product_Retailers {


	/** plugin version number */
	const VERSION = '1.0';

	/** plugin text domain */
	const TEXT_DOMAIN = 'wc-product-retailers';

	/** @var string the plugin path */
	private $plugin_path;

	/** @var string the plugin url */
	private $plugin_url;

	/** @var WC_Product_Retailers_List the admin retailers list screen */
	private $admin_retailers_list;

	/** @var WC_Product_Retailers_Edit the admin retailers edit screen */
	private $admin_retailers_edit;

	/** @var boolean set to try after the retailer dropdown is rendered on the product page */
	private $retailer_dropdown_rendered = false;


	/**
	 * Initializes the plugin
	 *
	 * @since 1.0
	 */
	public function __construct() {

		// include required files
		$this->includes();

		add_action( 'init', array( $this, 'init' ) );
		add_action( 'init', array( $this, 'include_template_functions' ), 25 );

		// render frontend embedded styles
		add_action( 'wp_print_styles',                array( $this, 'render_embedded_styles' ), 1 );

		// control the loop add to cart buttons for the product retailer products
		add_filter( 'woocommerce_is_purchasable',     array( $this, 'product_is_purchasable' ), 10, 2 );
		add_filter( 'not_purchasable_text',           array( $this, 'add_to_cart_text' ) );
		add_filter( 'out_of_stock_add_to_cart_text',  array( $this, 'add_to_cart_text' ) );
		add_filter( 'woocommerce_product_is_visible', array( $this, 'product_variation_is_visible' ), 1, 2 );

		// add the product retailers dropdown on the single product page (next to the 'add to cart' button if available)
		add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'add_retailer_dropdown' ) );
		add_action( 'woocommerce_single_product_summary',   array( $this, 'add_retailer_dropdown' ), 35 );

		// admin
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {

			// add a 'Configure' link to the plugin action links
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'add_plugin_setup_link' ) );

			// run every time
			$this->install();
		}
	}


	/**
	 * Initialize translation and taxonomy
	 *
	 * @since 1.0
	 */
	public function init() {

		$this->load_translation();

		WC_Product_Retailers_Taxonomy::initialize();
	}


	/**
	 * Include required files
	 *
	 * @since 1.0
	 */
	private function includes() {

		require( 'classes/class-wc-product-retailers-product.php' );
		require( 'classes/class-wc-product-retailers-taxonomy.php' );
		require( 'classes/class-wc-retailer.php' );

		if ( is_admin() )
			$this->admin_includes();
	}


	/**
	 * Include required admin files
	 *
	 * @since 1.0
	 */
	private function admin_includes() {

		require( 'classes/admin/class-wc-product-retailers-admin.php' );
		$this->admin = new WC_Product_Retailers_Admin();

		require( 'classes/admin/class-wc-product-retailers-list.php' );
		$this->admin_retailers_list = new WC_Product_Retailers_List();

		require( 'classes/admin/class-wc-product-retailers-edit.php' );
		$this->admin_retailers_edit = new WC_Product_Retailers_Edit();
	}


	/**
	 * Function used to Init WooCommerce Product Retailers Template Functions
	 * This makes them pluggable by plugins and themes.
	 */
	public function include_template_functions() {
		include_once( 'woocommerce-product-retailers-template.php' );
	}


	/**
	 * Handle localization, WPML compatible
	 *
	 * @since 1.0
	 */
	private function load_translation() {

		// localization in the init action for WPML support
		load_plugin_textdomain( self::TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/' );
	}


	/** Admin methods ******************************************************/


	/**
	 * Return the plugin action links.  This will only be called if the plugin
	 * is active.
	 *
	 * @since 1.0
	 * @param array $actions associative array of action names to anchor tags
	 * @return array associative array of plugin action links
	 */
	public function add_plugin_setup_link( $actions ) {
		// add the link to the front of the actions list
		return ( array_merge( array( 'configure' => sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=woocommerce_settings' ), __( 'Configure', self::TEXT_DOMAIN ) ) ),
			$actions )
		);
	}


	/** Frontend methods ******************************************************/


	/**
	 * Renders the product retailers frontend button/select box styles
	 *
	 * @since 1.0
	 */
	public function render_embedded_styles() {
		global $post;

		if ( is_product() ) {
			$product = get_product( $post->ID );

			if ( WC_Product_Retailers_Product::has_retailers( $product ) ) {
				echo '<style type="text/css">.wc-product-retailers-wrap { padding:1em 0;clear:both };</style>';
			}
		}
	}


	/**
	 * Make product variations visible even if they don't have a price, as long
	 * as they are sold only through retailers.
	 *
	 * This is one of the few times where we are altering this filter in a
	 * positive manner, and so we try to hook into it first.
	 *
	 * @since 1.0
	 * @param boolean $visible whether the product is visible
	 * @param int $product_id the product id
	 * @return boolean true if the product is visible, false otherwise.
	 */
	public function product_variation_is_visible( $visible, $product_id ) {

		$product = get_product( $product_id );

		if ( $product->is_type( 'variable' ) ) {
			if ( WC_Product_Retailers_Product::is_retailer_only_purchase( $product ) ) $visible = true;
		}

		return $visible;
	}


	/**
	 * Marks "retailer only" products as not purchasable
	 *
	 * @since 1.0
	 * @param boolean $purchasable whether the product is purchasable
	 * @param WC_Product $product the product
	 * @return boolean true if $product is purchasable, false otherwise
	 */
	public function product_is_purchasable( $purchasable, $product ) {

		if ( WC_Product_Retailers_Product::is_retailer_only_purchase( $product ) ) {
			$purchasable = false;
		}

		return $purchasable;
	}


	/**
	 * Modify the 'add to cart' text for simple product retailer products which
	 * are sold only through retailers to display the catalog button text.
	 * This is because the customer must select a retailer to purchase
	 *
	 * @since 1.0
	 * @param string $label the 'add to cart' label
	 * @return string the 'add to cart' label
	 */
	public function add_to_cart_text( $label ) {
		global $product;

		if ( $product->is_type( 'simple' ) && WC_Product_Retailers_Product::is_retailer_only_purchase( $product ) && WC_Product_Retailers_Product::has_retailers( $product ) )
			$label = __( WC_Product_Retailers_Product::get_catalog_button_text( $product ), self::TEXT_DOMAIN );

		return $label;
	}


	/**
	 * Display the product retailers drop down box
	 *
	 * @since 1.0
	 */
	public function add_retailer_dropdown() {
		global $product;

		// get any product retailers
		$retailers = WC_Product_Retailers_Product::get_product_retailers( $product );

		// only add dropdown if retailers have been assigned and it hasn't already been displayed
		if ( $this->retailer_dropdown_rendered || empty( $retailers ) )
			return;

		$this->retailer_dropdown_rendered = true;

		woocommerce_single_product_product_retailers( $product, $retailers );
	}


	/** Helper methods ******************************************************/


	/**
	 * Gets the global default Product Button text default
	 *
	 * @since 1.0
	 * @return string the default product button text
	 */
	public function get_product_button_text() {
		return get_option( 'wc_product_retailers_product_button_text' );
	}


	/**
	 * Gets the global default Catalog Button text default
	 *
	 * @since 1.0
	 * @return string the default product button text
	 */
	public function get_catalog_button_text() {
		return get_option( 'wc_product_retailers_catalog_button_text' );
	}


	/**
	 * Gets the absolute plugin path without a trailing slash
	 *
	 * @since 1.0
	 * @return string plugin path
	 */
	public function get_plugin_path() {

		if ( $this->plugin_path )
			return $this->plugin_path;

		return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}


	/**
	 * Gets the plugin url without a trailing slash
	 *
	 * @since 1.0
	 * @return string the plugin url
	 */
	public function get_plugin_url() {

		if ( $this->plugin_url )
			return $this->plugin_url;

		return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
	}


	/** Lifecycle methods ******************************************************/


	/**
	 * Run every time.  Used since the activation hook is not executed when updating a plugin
	 *
	 * @since 1.0
	 */
	private function install() {

		// get current version to check for upgrade
		$installed_version = get_option( 'wc_product_retailers_version' );

		// install
		if ( ! $installed_version ) {

			// install default settings
			foreach ( WC_Product_Retailers_Admin::get_global_settings() as $setting ) {

				if ( isset( $setting['default'] ) )
					update_option( $setting['id'], $setting['default'] );
			}

		}


		// upgrade if installed version lower than plugin version
		if ( -1 === version_compare( $installed_version, self::VERSION ) )
			$this->upgrade( $installed_version );
	}


	/**
	 * Perform any version-related changes.
	 *
	 * @since 1.0
	 * @param int $installed_version the currently installed version of the plugin
	 */
	private function upgrade( $installed_version ) {

		// update the installed version option
		update_option( 'wc_product_retailers_version', self::VERSION );
	}


} // end \WC_Product_Retailers class
