<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		Justin Lancy
 * @package 	TravelerTech/Templates
 * @version     1..0
 
 do_action('woocommerce_product_thumbnails'); 
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); ?>


		<?php while ( have_posts() ) : the_post(); ?>

<div class="col-full">	
	<!-- CATEGORY and TAGS --!>	
	<div class="product_meta">

		<?php do_action( 'woocommerce_product_meta_start' ); ?>

		<?php if ( $product->is_type( array( 'simple', 'variable' ) ) && get_option( 'woocommerce_enable_sku' ) == 'yes' && $product->get_sku() ) : ?>
			<span itemprop="productID" class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo $product->get_sku(); ?></span>.</span>
		<?php endif; ?>

		<?php
			$size = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
			echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $size, 'woocommerce' ) . ' ', '.</span>' );
		?>

		<?php
			$size = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
			echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $size, 'woocommerce' ) . ' ', '.</span>' );
		?>

		<?php do_action( 'woocommerce_product_meta_end' ); ?>

	</div> <!-- product_meta--!>

	<!-- REVIEW HEADER--!>	
	<div class="review-head">

		<!-- PRODUCT IMAGE, TITLE OVERLAY, AND DESCRIPTION --!>
		<div class="product_image">
			<!-- PRODUCT NAME AND SUMMARY --!>
			<div class="product_name">
				<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>
			</div>
			<div class="product_description">
				<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
			</div>

			<!-- PRODUCT PHOTO --!>
			<?php if ( has_post_thumbnail() ) : ?>
				<a itemprop="image" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="zoom" rel="thumbnails" title="<?php echo get_the_title( get_post_thumbnail_id() ); ?>"><?php echo get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) ) ?></a>
			<?php else : ?>
				<img src="<?php echo woocommerce_placeholder_img_src(); ?>" alt="Placeholder" />
			<?php endif; ?>
		</div> <!-- product_image --!>

		<!-- PRODUCT RATINGS, CART, AND AUTHOR --!>
		<section class="product_details">

			<!-- REVIEW SCORES --!>
			<aside id="post-review">
				<h4>Traveler Tech Rating:</h4>
				<?php echo woocommerce_template_loop_rating(); ?>
				<br />
				<h4>Average User Rating:</h4>
				<?php echo woocommerce_template_loop_rating(); ?>
			</aside>
	
			<!-- RETAILERS --!>
			<aside id="buy">
				<h4>Find It Here:</h4>
				<?php
				// Availability
				$availability = $product->get_availability();
				if ($availability['availability']) :
				echo apply_filters( 'woocommerce_stock_html', '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>', $availability['availability'] );
				endif;
				?>

				<?php if ( $product->is_in_stock() ) : ?>

				<?php do_action('woocommerce_before_add_to_cart_form'); ?>

				<form action="<?php echo esc_url( $product->add_to_cart_url() ); ?>" class="cart" method="post" enctype='multipart/form-data'>
	 				<?php do_action('woocommerce_after_add_to_cart_button'); ?>
				</form>

				<?php do_action('woocommerce_after_add_to_cart_form'); ?>

				<?php endif; ?>
			</aside> <!-- BUY -->

			<!-- AUTHOR --!>
			<aside id="post-author">
				<div class="profile-image"><?php echo get_avatar( $author_id, '80' ); ?></div>
				<div class="profile-content">
					Reviewed by: 
					<h4><a href="<?php echo get_the_author_meta( 'user_url', $author_id ); ?>"><?php printf( esc_attr__( '%s', 'woothemes' ), get_the_author_meta( 'display_name', $author_id ) ); ?></a></h4>
					<?php echo get_the_author_meta( 'description', $author_id ); ?>
					<?php if ( is_singular() ) { ?>
		
					<!-- AUTHOR PROFILE LINKS--!>
					<div class="profile-link">
						<ul class="social-links">
							<?php if ( get_the_author_meta( 'twitter' ) != '' )  { ?>
							<li><a class="social-twitter" href="https://twitter.com/<?php echo wp_kses( get_the_author_meta( 'twitter' ), null ); ?>"><?php printf( esc_attr__( 'Follow %s on Twitter', 'tuts_plus'), get_the_author() ); ?></a></li>
							<?php } ?>
							<?php if ( get_the_author_meta( 'facebook' ) != '' )  { ?>
							<li><a class="social-facebook" href="<?php echo esc_url( get_the_author_meta( 'facebook' ) ); ?>"><?php printf( esc_attr__( 'Follow %s on Facebook', 'tuts_plus'), get_the_author() ); ?></a></li>
           					<?php } ?>
							<?php if ( get_the_author_meta( 'linkedin' ) != '' )  { ?>
               				<li><a class="social-linkedin" href="<?php echo esc_url( get_the_author_meta( 'linkedin' ) ); ?>"><?php printf( esc_attr__( 'Connect with %s on LinkedIn', 'tuts_plus'), get_the_author() ); ?></a></li>
           					<?php } ?>
           					<?php if ( get_the_author_meta( 'appdotnet' ) != '' )  { ?>
        					<li><a class="social-adn" href="<?php echo esc_url( get_the_author_meta( 'appdotnet' ) ); ?>"><?php printf( esc_attr__( 'Connect with %s on App.net', 'tuts_plus'), get_the_author() ); ?></a></li>
        					<?php } ?>
							<?php if ( get_the_author_meta( 'googleplus' ) != '' )  { ?>
							<li><a class="social-google" href="<?php echo esc_url( get_the_author_meta( 'googleplus' ) ); ?>"><?php printf( esc_attr__( 'Follow %s on Google+', 'tuts_plus'), get_the_author() ); ?></a></li>
							<?php } ?>
						</ul>
					
						<!-- POSTS BY AUTHOR --!>
						<dl>
							<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ); ?>">
							<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'woothemes' ), get_the_author_meta( 'display_name', $author_id ) ); ?>
							</a>
						</dl>
					</div><!--#profile-link-->
					<?php } ?>
				</div> <!-- PROFILE-CONTENT --!>
				<div class="fix"></div>
			</aside> <!-- POST AUTHOR --!>
		</section> <!-- product_details --!>
	</div> <!-- REVIEW-head --!>


	<!-- REVIEW AND INFO TABS --!>
	<?php do_action( 'woocommerce_after_single_product_summary' ); ?>

</div> <!-- col-full --!>

<?php endwhile; // end of the loop. ?>

<?php get_footer('shop'); ?>